<?php


namespace App\Controller;


use App\Model\User;
use Kiri\Router\Base\AuthorizationInterface;

/**
 * Class Controller
 * @package App\Http\Controller
 */
class Controller extends \Kiri\Router\Base\Controller
{


	/**
	 * @return User|AuthorizationInterface
	 */
	protected function getCurrentOnlineUser(): User|AuthorizationInterface
    {
		return $this->request->authority;
	}


}
