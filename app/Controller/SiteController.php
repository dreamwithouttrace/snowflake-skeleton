<?php


namespace App\Controller;




use Psr\Http\Message\ResponseInterface;

/**
 * Class SiteController
 * @package App\Http
 */
class SiteController extends Controller
{


	/**
	 * @return ResponseInterface
	 */
	public function index(): ResponseInterface
	{
		return $this->response->html('hello word');
	}


}
