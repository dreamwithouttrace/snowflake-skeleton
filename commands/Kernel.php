<?php

namespace Commands;

/**
 * Class Kernel
 * @package Commands
 */
class Kernel implements \Kiri\Abstracts\Kernel
{

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected array $commands = [
        GiiRequestFilterCommand::class,
        GiiModelCommand::class,
        GiiControllerCommand::class,
        BackViewController::class
    ];


	/**
	 * @return array
	 */
	public function getCommands(): array
	{
		return $this->commands;
	}

}
