#!/usr/bin/env php
<?php

error_reporting(E_ALL);

const APP_PATH = __DIR__ . '/';
ini_set('display_errors', 'stderr');
ini_set('swoole.use_shortname', 'On');
date_default_timezone_set('Asia/Shanghai');

ini_set('memory_limit', '4G');

use Commands\Kernel;
use Database\DatabasesProviders;
use Kiri\Application;
use Kiri\Server\ServerProviders;
try {
    require_once APP_PATH . 'vendor/autoload.php';

    $application = Kiri::getDi()->get(Application::class);
    $application->import(DatabasesProviders::class);
    $application->import(ServerProviders::class);
    $application->commands(new Kernel());

    $application->execute($argv);
} catch (Throwable $exception) {
    echo throwable($exception);
}