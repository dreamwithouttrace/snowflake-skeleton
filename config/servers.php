<?php

use Components\AppExceptionHandler;
use Kiri\Server\Abstracts\AsyncServer;
use Kiri\Server\Constant;
use Kiri\Server\Handler\OnPipeMessage;
use Kiri\Server\Handler\OnRequest;
use Kiri\Server\Handler\OnServer;
use Kiri\Server\Handler\OnServerManager;
use Kiri\Server\Handler\OnServerWorker;
use Kiri\Router\ContentType;


return [
    'response'  => [
        'format' => ContentType::JSON,
    ],
    "exception" => [
        "task" => AppExceptionHandler::class,
        "http" => AppExceptionHandler::class,
        'tcp'  => AppExceptionHandler::class,
        'udp'  => AppExceptionHandler::class
    ],
    "server"    => [
        'type'     => AsyncServer::class,
        'settings' => [
            Constant::OPTION_LOG_FILE         => APP_PATH . 'storage/system.log',
            Constant::OPTION_LOG_LEVEL        => SWOOLE_LOG_WARNING,
            Constant::OPTION_STATS_FILE       => APP_PATH . 'storage/stats.log',
            Constant::OPTION_DISPATCH_MODE    => 3,
            Constant::OPTION_TASK_WORKER_NUM  => 5,
            Constant::OPTION_ENABLE_COROUTINE => true,
            Constant::OPTION_DAEMONIZE        => 0,
            Constant::OPTION_MAX_WAIT_TIME    => 60,
            Constant::OPTION_MAX_COROUTINE    => 2000000,
            Constant::OPTION_RELOAD_ASYNC     => true,
            Constant::OPTION_OPEN_TCP_NODELAY => true,
            Constant::OPTION_TCP_DEFER_ACCEPT => true,
            Constant::OPTION_TCP_FASTOPEN     => true,
            Constant::OPTION_WORKER_NUM       => swoole_cpu_num(),
            Constant::OPTION_REACTOR_NUM      => swoole_cpu_num() / 2
        ],
        'events'   => [
            Constant::PIPE_MESSAGE  => [OnPipeMessage::class, 'onPipeMessage'],
            Constant::START         => [OnServer::class, 'onStart'],
            Constant::MANAGER_START => [OnServerManager::class, 'onManagerStart'],
            Constant::WORKER_START  => [OnServerWorker::class, 'onWorkerStart'],
            Constant::WORKER_EXIT   => [OnServerWorker::class, 'onWorkerExit'],
            Constant::WORKER_ERROR  => [OnServerWorker::class, 'onWorkerError'],
            Constant::WORKER_STOP   => [OnServerWorker::class, 'onWorkerStop'],
        ],
        'ports'    => [
            [
                'type'     => Constant::SERVER_TYPE_HTTP,
                'host'     => '0.0.0.0',
                'port'     => 6604,
                'mode'     => SWOOLE_PROCESS,
                'socket'   => SWOOLE_SOCK_TCP,
                'settings' => [
                ],
                'events'   => [
                    Constant::REQUEST => [OnRequest::class, 'onRequest'],
                ]
            ]
        ]
    ]
];