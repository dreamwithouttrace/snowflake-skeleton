<?php



return [

	'ssl' => [
		'public'  => '-----BEGIN PUBLIC KEY-----
-----END PUBLIC KEY-----',

		'private' => '-----BEGIN RSA PRIVATE KEY-----
-----END RSA PRIVATE KEY-----',
	],


	'jwt' => [
		'scene' => 'application',
		'timeout' => 7200,
		'iv'      => '',
		'key'     => 'application.encrypt.scene',
	]

];
