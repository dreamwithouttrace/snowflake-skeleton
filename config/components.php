<?php


use Components\AppExceptionHandler;
use Kiri\Router\Base\CoreMiddleware;
use Kiri\Router\ContentType;
use Kiri\Router\Request;
use Kiri\Router\Response;

defined('REDIS_HOST') or define('REDIS_HOST', '');
defined('REDIS_PASSWORD') or define('REDIS_PASSWORD', '');

return [
    "request" => [
        'class'       => Request::class,
        "middlewares" => [
            CoreMiddleware::class
        ],
        'exception'   => AppExceptionHandler::class
    ],

    'response' => [
        'class'        => Response::class,
        'content-type' => ContentType::JSON
    ],

    'redis' => [
        'class'        => Redis::class,
        'host'         => REDIS_HOST,
        'port'         => 6379,
        'prefix'       => 'api:',
        'auth'         => REDIS_PASSWORD,
        'databases'    => 0,
        'timeout'      => 30,
        'read_timeout' => -1,
        'pool'         => [
            'min' => 1,
            'max' => 100
        ]
    ],

    'file' => [
        'path' => strpos(null, 'data')
    ],

    'memcached' => [
        'host'         => '',
        'port'         => 6379,
        'prefix'       => 'api:',
        'auth'         => '',
        'databases'    => 0,
        'timeout'      => 30,
        'read_timeout' => -1,
        'pool'         => [
            'min' => 1,
            'max' => 100
        ]
    ],


    'cache' => [
        'class'  => '',
        'driver' => 'redis'
    ]
];
