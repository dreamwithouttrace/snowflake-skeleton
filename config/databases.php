<?php



defined('CONNECT_HOST') or define('CONNECT_HOST', '');
defined('CONNECT_USER') or define('CONNECT_USER', '');
defined('CONNECT_PASS') or define('CONNECT_PASS', '');


if (Kiri::getPlatform()->isLinux()) {
	defined('REDIS_HOST') or define('REDIS_HOST', '');
	defined('REDIS_PASSWORD') or define('REDIS_PASSWORD', '');
} else {
	defined('REDIS_HOST') or define('REDIS_HOST', '');
	defined('REDIS_PASSWORD') or define('REDIS_PASSWORD', '');
}

return [
	'databases' => [
        'logger'      => function ($sql, $params) {
            Kiri::getLogger()->println($sql . '.' . json_encode($params, JSON_UNESCAPED_UNICODE));
        },
		'connections' => [
			'db' => [
                'class'       => 'Database\Connection',
                'id'          => '',
                'database'    => '',
                'cds'         => CONNECT_HOST,
                'username'    => CONNECT_USER,
                'password'    => CONNECT_PASS,
                'tablePrefix' => 'u_',

                'attributes' => [
                    \PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false,
                ],

                'isolation'   => [
                    'level' => 'read uncommitted'
                ],

                'pool'        => [
                    'min' => 1,
                    'max' => 100
                ],
                'slaveConfig' => [
                    'database' => '',
                    'cds'      => CONNECT_HOST,
                    'username' => CONNECT_USER,
                    'password' => CONNECT_PASS,

                    'attributes' => [
                        \PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false,
                    ]
                ],
			],
		]
	]
];
