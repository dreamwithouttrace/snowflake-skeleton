<?php

use Monolog\Logger;

return [
    'id'   => 'snowflake-skeleton',
    'http' => [
        'namespace' => 'App\\Http\\Controller',
    ],

    'reload' => [
        'hot'    => true,
        'listen' => [APP_PATH . 'app', APP_PATH . 'routes']
    ],

    'log' => [
        'level' => [
            Logger::EMERGENCY,
            Logger::CRITICAL,
            Logger::NOTICE,
            Logger::INFO,
            Logger::DEBUG,
            Logger::ALERT,
            Logger::ERROR,
            Logger::WARNING,
        ]
    ],


    'environment' => 'dev',

    'email' => [
        'enable'   => false,
        'send'     => [
            'address'  => '',
            'nickname' => '',
        ],
        'receive'  => [
            [
                'address'  => '',
                'nickname' => '',
            ]
        ],
        'host'     => 'smtp.163.com',
        'port'     => 465,
        'username' => '',
        'password' => '',
    ],

    'oss' => [
        'accessKey'    => '',
        'accessSecret' => '',
        'endpoint'     => '',
        'bucket'       => '',
        'domain'       => '',
    ],
];
