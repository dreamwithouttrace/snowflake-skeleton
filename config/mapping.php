<?php


use app\Model\User;
use Kiri\Router\Base\AuthorizationInterface;
use Kiri\Router\Request;
use Kiri\Router\Response;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;


return [
    'mapping' => [
        AuthorizationInterface::class => User::class,
        RequestInterface::class       => Request::class,
        ResponseInterface::class      => Response::class,
    ]
];